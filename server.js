// Use Express
var express = require("express");
// Use body-parser
var bodyParser = require("body-parser");
// Use MongoDB
var mongodb = require("mongodb");
var ObjectID = mongodb.ObjectID;
// The database variable
var database;
// The products collection
var PRODUCTS_COLLECTION = "products";

// Create new instance of the express server
var app = express();

// Define the JSON parser as a default way
// to consume and produce data through the
// exposed APIs
app.use(bodyParser.json());

// Create link to Angular build directory
// The `ng build` command will save the result
// under the `dist` folder.
var distDir = __dirname + "/dist/";
app.use(express.static(distDir));
// Local database URI.
const LOCAL_DATABASE = "mongodb://localhost:27017/app";
// Local port.
const LOCAL_PORT = 8080;

// Init the server
mongodb.MongoClient.connect(process.env.MONGODB_URI || LOCAL_DATABASE,
    {
        useUnifiedTopology: true,
        useNewUrlParser: true,
    }, function (error, client) {

        // Check if there are any problems with the connection to MongoDB database.
        if (error) {
            console.log(error);
            process.exit(1);
        }

        // Save database object from the callback for reuse.
        database = client.db();
        console.log("Database connection done.");

        // Initialize the app.
        var server = app.listen(process.env.PORT || LOCAL_PORT, function () {
            var port = server.address().port;
            console.log("App now running on port", port);
        });
    });


app.get("/api/status", function (req, res) {
    res.status(200).json({ status: "UP" });
});


app.post("/api/login", function (req, res) {
  const form = req.body;

  if (!form.login) {
        manageError(res, "Invalid form input", "Login is mandatory.", 400);
    } else if (!form.password) {
        manageError(res, "Invalid form input", "Password is mandatory.", 400);
    } else {
        database.collection('users').findOne(form, function (err, doc) {
            if (err || doc === null) {
                manageError(res, err?.message, "Failed to login.");
            } else {
                res.status(200).json({ token: doc._id, login: form.login });
            }
        });
    }
});
app.post("/api/register", function (req, res) {
  const form = req.body;

  if (!form.login) {
        manageError(res, "Invalid form input", "Login is mandatory.", 400);
    } else if (!form.password) {
        manageError(res, "Invalid form input", "Password is mandatory.", 400);
    } else {
        database.collection('users').insertOne({login: form.login, password: form.password, publicKey: form.publicKey}, function (err, doc) {
            if (err) {
                manageError(res, err.message, "Failed to create new user.");
            } else {
              const online = Array.from(userSockets.keys());
              io.emit('newUser', {login: form.login, isOnline: online.includes(form.login), publicKey: form.publicKey});
              res.status(201).json({ token: doc._id, login: form.login });
            }

        });
    }
});

app.post("/api/users", async (req, res) => {
  var id = new ObjectID(req.body?.id);
  if (!id) {
    manageError(res, "Invalid form input", "ID is mandatory.", 400);
    return;
  }

    var user = await database.collection('users').findOne({_id: id});
  if (!user) {

    manageError(res, "Invalid form input", "Not logged in", 400);
    return;
  }


  const cursor = database.collection('users').find();

  if ((await cursor.count()) === 0) {
    res.status(200).json([]);
    return;
  }

  const arr = await cursor.toArray();

  const online = Array.from(userSockets.keys());
  res.status(200).json(arr.filter(val => val.login !== user.login).map(user => {
    if (online.includes(user.login)) {
      return {login: user.login, isOnline: true, publicKey: user.publicKey};
    }
    return {login: user.login, isOnline: false, publicKey: user.publicKey};
  }));
})

app.post("/api/conversations", async (req, res) => {
  var id = new ObjectID(req.body?.id);
  if (!id) {
    manageError(res, "Invalid form input", "ID is mandatory.", 400);
    return;
  }

  var user = await database.collection('users').findOne({_id: id});
  if (!user) {

    manageError(res, "Invalid form input", "Not logged in", 400);
    return;
  }


  const cursor = database.collection('conversations').find();

  if ((await cursor.count()) === 0) {
    res.status(200).json([]);
    return;
  }

  const arr = await cursor.toArray();

  res.status(200).json(arr.filter(val => val.user1 === user.login || val.user2 === user.login));
})



const http = require('http');
const server = http.createServer(app);
const { Server } = require("socket.io");
const io = new Server(server, {path: '/api/socket',
  cors: {
    origin: "http://localhost:4200",
    credentials: false
  },
  allowEIO3: true
});

var userSockets = new Map();

io.on('connection', async (socket) => {
  console.log('a user connected');
  const user = await database.collection('users').findOne({_id: ObjectID(socket.handshake.query.id)});
  if (!user) return;

  userSockets.set(user.login, socket);
  socket.broadcast.emit('userOnline', {login: user.login});


  socket.on('disconnect', () => {
    console.log('user disconnected');
    socket.broadcast.emit('userOffline', {login: user.login});
    userSockets.delete(user.login);
  });

  socket.on('client:selectConversation', async (s) => {
    var conversation;
    var conversationUser1 = await database.collection('conversations').findOne({user1: s.login, user2: s.receiver});

    if (conversationUser1) {
      conversation = conversationUser1;
    } else {
      var conversationUser2 = await database.collection('conversations').findOne({user2: s.login, user1: s.receiver});

      if (conversationUser2) {
        conversation = conversationUser2;
      } else {
        conversation = (await database.collection('conversations').insertOne({user1: s.login, user2: s.receiver, messages: []})).ops[0];
      }
    }

    userSockets.get(s.login)?.emit('conversation', conversation);
    userSockets.get(s.receiver)?.emit('conversation', conversation);
  } )

  socket.on('client:newMessage', async (s) => {
    var conversation = await database.collection('conversations').findOne({_id: ObjectID(s.conversationId)});

    conversation.messages.push({body: s.body, login: s.login});
    await database.collection('conversations').findOneAndUpdate({_id: ObjectID(s.conversationId)}, {$set: {messages: conversation.messages}});

    userSockets.get(conversation.user1)?.emit('newMsg', {body: s.body, login: s.login, _id: s.conversationId});
    userSockets.get(conversation.user2)?.emit('newMsg', {body: s.body, login: s.login, _id: s.conversationId});
  } )
});

server.listen(3500);

// Errors handler.
function manageError(res, reason, message, code) {
    console.log("Error: " + reason);
    res.status(code || 500).json({ "error": message });
}

