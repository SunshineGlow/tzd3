import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import {ActivatedRouteSnapshot, Router, ActivatedRoute} from '@angular/router';
import {MessageService} from 'primeng/api';

import * as Forge from 'node-forge';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  public user$ = new BehaviorSubject<User>(null);
  public users$ = new BehaviorSubject<User[]>(null);
  public conversations = new Map<string, Conversation>();
  public currentConversation$ = new BehaviorSubject<Conversation>(null);
  public currentUserLogin;

  public privateKey: Forge.pki.PrivateKey;
  public publicKey: Forge.pki.PublicKey;

  constructor(private http: HttpClient, private router: Router, private activatedRoute: ActivatedRoute, private messageService: MessageService) {
    const fromLocal = localStorage.getItem('user');
    if (!fromLocal && this.activatedRoute.snapshot.url) {
      this.router.navigateByUrl('/login');
    } else {
      const user = JSON.parse(fromLocal);
      this.user$.next(user);

      const publicKey = localStorage.getItem(`${user.login}_publicKey`);
      const privateKey = localStorage.getItem(`${user.login}_privateKey`);

      this.publicKey = Forge.pki.publicKeyFromPem(publicKey);
      this.privateKey = Forge.pki.decryptRsaPrivateKey(privateKey);
      this.router.navigateByUrl('/all');
    }
  }

  public getConversationByReceiver(login) {
    const c = Array.from(this.conversations.values());
    return c.find(res => res.receiverLogin === login);
  }

  public login(login: string, password: string): void {
    this.http.post('/api/login', {login, password: Forge.md.sha256.create().update(password)}).pipe().subscribe((res: User) => {
      this.messageService.add({severity:'success', summary:'Success log in', life: 10000});
      localStorage.setItem('user', JSON.stringify(res));

      const publicKey = localStorage.getItem(`${login}_publicKey`);
      const privateKey = localStorage.getItem(`${login}_privateKey`);

      this.publicKey = Forge.pki.publicKeyFromPem(publicKey);
      this.privateKey = Forge.pki.decryptRsaPrivateKey(privateKey);

      this.user$.next(res);
      this.router.navigateByUrl('/all');
    }, error => {
      this.messageService.add({severity:'error', summary: error.error.error, life: 10000});
    })
  }

  public register(login: string, password: string): void {
    const rsa = Forge.pki.rsa;
    rsa.generateKeyPair({bits: 2048, workers: 2}, (err, keypair) => {
      const publicKey = Forge.pki.publicKeyToRSAPublicKeyPem(keypair.publicKey);
      const privateKey = Forge.pki.privateKeyToPem(keypair.privateKey);

      localStorage.setItem(`${login}_publicKey`, publicKey);
      localStorage.setItem(`${login}_privateKey`, privateKey);
      this.http.post('/api/register', {login, publicKey: publicKey, password: Forge.md.sha256.create().update(password)}).pipe().subscribe(res => {
        this.messageService.add({severity:'success', summary:'Success register', life: 10000});
      }, error => {
        this.messageService.add({severity:'error', summary: error.error.error, life: 10000});
      })
    });


  }

  public getAll() {
    this.http.post('/api/users', {id: this.user$.getValue().token}).pipe().subscribe(res => {
      this.users$.next((res as any[]).map(user => {
        user.publicKey = Forge.pki.publicKeyFromPem(user.publicKey);
        return user as User;
      }));
    }, error => {
      this.messageService.add({severity:'error', summary: error.error.error, life: 10000});
    })
  }

  public hash(str: string) {
    let hash = 0;
    let i;
    let chr;
    if (str.length === 0) { return hash; }
    for (i = 0; i < str.length; i++) {
      chr   = str.charCodeAt(i);
      hash  = ((hash << 5) - hash) + chr;
      hash |= 0; // Convert to 32bit integer
    }
    return hash;
  }
}

export class User {
  public token?: string;
  public login: string;
  public isOnline: boolean;
  public publicKey: Forge.pki.PublicKey;
}

export class Conversation {
  public id: any;
  public receiverLogin: string;
  public messages$: BehaviorSubject<Message[]>;
}

export class Message {
  public body: string;
  public login: string;
}
