import { Injectable } from '@angular/core';
import {DefaultEventsMap} from 'socket.io/dist/typed-events';
import {BehaviorSubject} from 'rxjs';
import {Message, User, UserService} from './user.service';
import {filter} from 'rxjs/operators';
import {Socket} from 'ng-socket-io';
import * as Forge from 'node-forge';

@Injectable({
  providedIn: 'root'
})
export class EventService {

  private ioo: SocketOne

  constructor(private userService: UserService) {
    this.userService.user$.pipe(filter(user => !!user?.token)).subscribe((user) => this.connect(user));
  }

  public connect(user: User) {
    this.ioo = new SocketOne(user);

    this.ioo.fromEvent('userOnline').subscribe( ({login}) => {
      this.userService.users$.next(
        this.userService.users$.getValue().map(val => {
          if (val.login === login) {
            val.isOnline = true;
          }
          return val;
        })
      );
      }

    );
    this.ioo.on('userOffline', (({login}) => {
      this.userService.users$.next(
        this.userService.users$.getValue().map(val => {
          if (val.login === login) {
            val.isOnline = false;
          }
          return val;
        })
      );
    }));
    this.ioo.on('conversation', ((res) => {
      const receiverLogin = res.user1 === this.userService.user$.getValue().login ? res.user2 : res.user1;
      res.messages = res.messages.map(msg => {
        try {
          msg.body = (this.userService.privateKey as Forge.pki.rsa.PrivateKey).decrypt(msg.body);
        } catch (e) {
          msg.body = 'Your encrypted message';
        }
        return msg;
      });
      if (this.userService.conversations.has(res._id)) {
        this.userService.conversations.get(res._id).messages$.next(res.messages);
      } else {
        this.userService.conversations.set(res._id, {id: res._id, receiverLogin, messages$: new BehaviorSubject<Message[]>(res.messages)});
      }
      if (this.userService.currentUserLogin === receiverLogin) {
        this.userService.currentConversation$.next(this.userService.conversations.get(res._id));
      }
    }));
    this.ioo.on('newMsg', ((res) => {
      if (this.userService.conversations.has(res._id) && res.login !== this.userService.user$.getValue().login) {
        if ('decrypt' in this.userService.privateKey) {
          res.body = this.userService.privateKey.decrypt(res.body);
          const msgs$ = this.userService.conversations.get(res._id).messages$;
          const msgs = msgs$.getValue();
          msgs.push(res);
          msgs$.next(msgs);
        }
        return;
      }

      this.selectConversation({login: res.login});

    }));
    this.ioo.on('newUser', ((res) => {
      res.publicKey = Forge.pki.publicKeyFromPem(res.publicKey);
      const users = this.userService.users$.getValue();
      users.push(res);
      this.userService.users$.next(users);
    }));
  }

  public selectConversation(user) {
    this.ioo.emit('client:selectConversation', {login: this.userService.user$.getValue().login, receiver: user.login});
  }

  public send(model: { conversationId: any; body: any; login?: string; }) {
    const conversation = this.userService.conversations.get(model.conversationId);
    const msgs = conversation.messages$.getValue();
    msgs.push({body: model.body, login: model.login});
    conversation.messages$.next(msgs);
    const receiver = this.userService.users$.getValue().find(user => user.login === conversation.receiverLogin);
    if ('encrypt' in receiver.publicKey) {
      model.body = receiver.publicKey.encrypt(model.body);
      this.ioo.emit('client:newMessage', model);
    }
  }

}

export class SocketOne extends Socket {

  constructor(user) {
    super({ url: 'http://localhost:3500', options: {path: '/api/socket', query: {id: user.token}, withCredentials: false }});
  }
}
