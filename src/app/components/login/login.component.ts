import { Component, OnInit } from '@angular/core';
import {UserService} from '../../services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginProp: string;
  passwordProp: string;

  constructor(private userService: UserService) { }

  ngOnInit(): void {
  }

  login() {
    this.userService.login(this.loginProp, this.passwordProp);
  }

  register() {
    this.userService.register(this.loginProp, this.passwordProp);
  }
}
