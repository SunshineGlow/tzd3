import { Component, OnInit } from '@angular/core';
import {User, UserService} from '../../services/user.service';

@Component({
  selector: 'app-all',
  templateUrl: './all.component.html',
  styleUrls: ['./all.component.scss']
})
export class AllComponent implements OnInit {
  public user: User;

  constructor(private userService: UserService) { }

  ngOnInit(): void {
  }

}
