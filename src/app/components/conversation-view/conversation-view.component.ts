import {Component, Input, OnInit} from '@angular/core';
import {User, UserService} from '../../services/user.service';
import {EventService} from '../../services/event.service';

@Component({
  selector: 'app-conversation-view',
  templateUrl: './conversation-view.component.html',
  styleUrls: ['./conversation-view.component.css']
})
export class ConversationViewComponent implements OnInit {
  @Input() get chosenUser(): User {
    return this._chosenUser;
  }

  set chosenUser(value: User) {
    this._chosenUser = value;
    this.userService.currentUserLogin = value.login;
    const c = this.userService.getConversationByReceiver(value.login);
    if (c) {
      this.userService.currentConversation$.next(c);
    } else {
      this.eventService.selectConversation(value);
    }
  }
  private _chosenUser: User;

  public conversation$ = this.userService.currentConversation$;
  public msg: string;

  constructor(public userService: UserService, public eventService: EventService) { }

  ngOnInit(): void {
  }

  public send(): void {
    this.eventService.send({conversationId: this.conversation$.getValue().id, body: this.msg, login: this.userService.user$.getValue().login});
    this.msg = '';
  }

}
