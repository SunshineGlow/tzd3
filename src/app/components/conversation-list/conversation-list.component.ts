import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {User, UserService} from '../../services/user.service';
import {MessageService} from 'primeng/api';

@Component({
  selector: 'app-conversation-list',
  templateUrl: './conversation-list.component.html',
  styleUrls: ['./conversation-list.component.scss']
})
export class ConversationListComponent implements OnInit {
  users$ = this.userService.users$.asObservable();
  @Output() public userSelected = new EventEmitter();

  constructor(private userService: UserService, private messages: MessageService) { }

  ngOnInit(): void {
    this.userService.getAll();
  }

  selectUser(user: User) {
    // if (user.isOnline === false) {
    //   this.messages.add({severity: 'error', life: 10000, detail: `${user.login} is not online`});
    //   return;
    // }
    this.userSelected.emit(user);
  }
}
